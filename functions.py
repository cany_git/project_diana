import sys
import time
import wikipedia

def typping(input_answer, wait=0.05, max_char=70):
	c = 0
	texte = input_answer[0].upper() + input_answer[1:]
	texte = texte.split(" ")
	texte.append("\n")
	texte = [word + " " for word in texte]
	for word in texte :
		c += len(word)
		if c >= 70 :
			print()
			c = 0
		for char in word:
			time.sleep(wait)
			sys.stdout.write(char)
			sys.stdout.flush()


# def get_time_since_birth(birth_date):
# 	"""Birth date == str("d/m/y")"""
# 	m = 60
# 	h = 60*m
# 	day = 24*h
# 	year = 365.25*day
#
# 	d, m, y = [int(x) for x in birth_date.split("/")]
# 	s_birth = time.mktime((y,m,d+1, 0,0,0, 0,0,0))
#
# 	s_life_time = time.time() - s_birth
#
# 	# h_life_time = int(s_life_time /h)
# 	# d_life_time = int(s_life_time /day)
# 	y_life_time = int(s_life_time /year)
#
# 	d_y_life_time = int(s_life_time - y_life_time*year) // day
# 	h_y_life_time = (s_life_time - y_life_time*year - d_y_life_time*day) / h
#
# 	return y_life_time, d_y_life_time, round(h_y_life_time,2)

def wikisearch(info):
	wikipedia.set_lang("fr")
	return wikipedia.summary(info, sentences=1)

#
# if __name__ == "__main__":
# 	print(get_time_since_birth("22/12/1998"))
