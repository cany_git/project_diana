import os
from pandas import read_csv
import json

with open('data.json') as json_file:
    data = json.load(json_file)

with open('user.json') as json_file:
    user = json.load(json_file)


def tpm(text):
	data['tpm'] = []
	data['tpm'].append(text)

def is_name_in(text):
	"""Function to search if there is a name in text"""
	text = text.lower()

	df = read_csv(os.path.join("assets" ,"Prenoms.csv"), sep=";")
	words = text.split(" ")

	for word in words:
		if (word in list(df["prenom"])) or (words.index(word) == len(words)-1):
			return word


	return False

def is_known(name):
	name = name.lower()
	for people in user:
		if name in people.get('fname'):
			val = people.get('lname')
			return val

def save_id(text):
	text = text.lower()
	word = text.split(' ')
	user.append({
	    'fname': word[1],
        'lname': word[0],
        'birth': word[2]
	})
	with open('user.json', 'w', encoding='utf-8') as f:
		json.dump(user, f,sort_keys=True , ensure_ascii=False, indent=4)
	print(word)
	return True

def want_to_quit(text):
	text = text.lower()
	keywords = [
	"aurevoir",
	"quitter",
	"quit",
	"exit"
	]

	for word in keywords:
		if word in text.split(" "):
			return True

	return False

def want_wiki_info(info):
	keywords = [
	"qui est"]

	for word in keywords:
		if info.startswith(word):
			return True
	return False

# if __name__ == "__main__":
# 	print(want_to_quit("Aller aurevoir"))
# 	print(want_to_quit("Je veux rester"))

def oui(text):
	text = text.lower()

	for word in data['oui']:
		if word in text.split(" "):
			if data['tpm'][0] in data['oui']:
				pass
			else:
			    data['oui'].append(data['tpm'][0])
			with open('data.json', 'w', encoding='utf-8') as f:
				json.dump(data, f,sort_keys=True , ensure_ascii=False, indent=4)
			return True



	return False

def non(text):
	text = text.lower()

	for word in data['non']:
		if word in text.split(" "):
			if data['tpm'][0] in data['non']:
				pass
			else:
			    data['non'].append(data['tpm'][0])
			with open('data.json', 'w', encoding='utf-8') as f:
				json.dump(data, f,sort_keys=True , ensure_ascii=False, indent=4)
			return True



	return False
