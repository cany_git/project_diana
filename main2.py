from functions import *
from syntax_analysis import *
import json

run = True
variable_autoris = ["user_response"]

with open('arbre_questions.json') as json_file:
    questions = json.load(json_file)

question_active = questions["1"]

while run:
	var_texte = []
	for name in question_active["var"]:
		if name in variable_autoris:
			variable = locals()[name] #locals = variable locals
			var_texte.append(variable)

	print(question_active["texte"].format(*var_texte))
	user_response = input(">")

	if want_to_quit(user_response):
		run = False

	elif oui(user_response):
		question_active = questions[question_active["suivantes"]["oui"]]

	elif non(user_response):
		question_active = questions[question_active["suivantes"]["non"]]

	else:
		question_active = questions[question_active["suivantes"]["jsp"]]
