#coding:utf-8
import pygame
import time
from pygame.locals import *
from classes import *
from functions import *

win = pygame.display.set_mode((0,0), FULLSCREEN)

run = True
play = True
fps_controler = pygame.time.Clock()
fps = 20
pygame.mouse.set_visible(False)


stack_time = 0
nb_mot_s = 0.5 # nombre de mot qui pop / s de départ
vie = 15
speed_move = 1
nb_mot_s = 0.8


LetterCloud.init(win)
BARRE = TextBar(win)
time_lab = Label(win,text=str(0), pos=(1200,10), color=[100,100,100])

mot_min = Label(win,text="", pos=[win.get_rect().w//2-50, win.get_rect().h//3], size=35, color=[200,200,200])
lettre_min = Label(win,text="", pos=[win.get_rect().w//2-50,win.get_rect().h*2//3], size=35, color=[200,200,200])
new_r = Label(win,text="Nouveau record !", pos=[win.get_rect().w//2-50,15], size=35)
new_r.centerize("x")



t0 = time.time()
while run:
	while play: # Il faut pas prendre exemple sur les while imbriqués, il fallait faire une classe Page mais j'avais la flemme
		win.fill((0,0,0))
		events = pygame.event.get()
		for event in events:
			if event.type == QUIT:
				play = False
			
			elif event.type == KEYDOWN:
				if event.key == K_ESCAPE:
					play = False

		for label in LetterCloud.liste_labels:
			label.move(speed_move)
			label.show()
			if not label.exploding and not label.fading and label.pos[0] + label.render.get_rect().w > win.get_rect().w:
				vie -= 1 

				if vie <= 0:
					LetterCloud.fade()
				else:
					label.explode()

		LetterCloud.show()
		BARRE.show(events)


		time_lab.set_text(str(round(time.time()-t0, 2))+" s")
		time_lab.show()

		pygame.display.flip()
		fps_controler.tick(fps)
		
		# génération des mots

		if stack_time > 1/nb_mot_s and vie > 0:
			LetterCloud.pop()
			stack_time = 0
		stack_time += 1/fps


			
		if not LetterCloud.liste_labels:
			play = False


	records = load_records()
	# mise à jour des records ========
	records[0] = max(round(len(BARRE.validate)*60 / (time.time()-t0), 2), records[0])
	records[1] = max(round(sum([len(w) for w in BARRE.validate])*60 / (time.time()-t0), 2), records[1])
	nr = records != load_records()

	mot_min.set_text(str(round(len(BARRE.validate)*60 / (time.time()-t0), 2)) + f" mots / minutes (record = {load_records()[0]})")
	lettre_min.set_text(str(round(sum([len(w) for w in BARRE.validate])*60 / (time.time()-t0), 2)) + f" lettres / minutes (record = {load_records()[1]})")
	
	save_records(*records)

	mot_min.centerize("x")
	lettre_min.centerize("x")

	menu = True
	while menu:
		win.fill((0,0,0))
		events = pygame.event.get()
		for event in events:
			if event.type == QUIT:
				run = False
				play = False
				menu = False

			elif event.type == KEYDOWN:
				if event.key == K_SPACE:
					play = True
					menu = False
					LetterCloud.init(win)
					t0 = time.time()

				elif event.key == K_ESCAPE:
					play = False
					menu = False
					run = False


		mot_min.show()
		lettre_min.show()
		if nr:
			new_r.show()

		fps_controler.tick(fps)
		pygame.display.flip()