from random import *
import time
import sys
from progress.bar import *
from progress.spinner import *


# Spinning cursor
def spinning_cursor():
  while True:
    for cursor in '\\|/-':
      time.sleep(0.1)
      # Use '\r' to move cursor back to line beginning
      # Or use '\b' to erase the last character
      sys.stdout.write('\r{}'.format(cursor))
      # Force Python to write data into terminal.
      sys.stdout.flush()

i = 0

s1 = random ()
print(s1)

s2 = random ()
print(s2)

class SlowBar(IncrementalBar):
    suffix = '%(remaining_hours)d hours remaining'' %(percent)d%%'
    @property
    def remaining_hours(self):
        return self.eta // 3600

bar = SlowBar('Loading',max = 1000000)



while True:
    if s1 != s2:

        i=i + 1

        if s1 == s2:
            print(s1,s2)

        if i > 1000000:
            bar.finish()
            print('finish')
            break

        bar.next()
