import os
import time

cmdA = "stty -F /dev/ttyUSB0 4800 istrip cs7 parenb -parodd brkint \
ignpar icrnl ixon ixany opost onlcr cread hupcl isig icanon \
echo echoe echok"
cmdB = "echo 'Hello World' > /dev/ttyUSB0"
cmdC = "sudo systemctl daemon-reload"
cmdD = "sudo systemctl start serial-getty@ttyUSB0.service"

time.sleep(1)
os.system(cmdA)

time.sleep(2)
os.system(cmdB)

time.sleep(2)
os.system(cmdC)

time.sleep(2)
os.system(cmdD)
